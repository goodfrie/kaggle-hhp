#! /usr/bin/env Rscript

# misc.r
# INFO 290 Project
# Scott Goodfriend - April 21, 2021
    
options("repos" = "http://cran.us.r-project.org");

# http://tolstoy.newcastle.edu.au/R/help/03b/6750.html
increment <- function(a, b = 1) {
    eval.parent(substitute(a <- a + b));
}



# Create a labeled table.
getNamedTable <- function(data, column_names) {
    colnames(data) <- column_names;
    data;
}


# Create a formula.
createFormula <- function(lhs, rhs_list) {
    lhs_str <- paste(lhs, " ~ ", sep = "");
    rhs_str <- paste(rhs_list, collapse = "+");
    fmla <- as.formula(paste(lhs_str, rhs_str, sep = ""));
    fmla;
}


# Create loop indexes array.
loop <- function(sz) {
    indexes <- c();

    if (sz > 0) {
        indexes <- 1:sz;
    }

    indexes;
}


# Euclidian normalization (make vector of length 1).
euclidianNormalize <- function(row) {
    distance <- sqrt(sum(row**2));
    if (distance > 0) {
        row <- row / distance;
    }

    row;
}
