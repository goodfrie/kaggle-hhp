#! /usr/bin/env Rscript

# random_forest.r
# INFO 290 Project
# Scott Goodfriend - April 21, 2012

options("repos" = "http://cran.us.r-project.org");
install.packages('randomForest');
library('randomForest');

source('input.r');
source('output.r');

# Select row building methodology.
source('pcg_short.r');
# Select data set.
filename <- '../../data.json';


# Random Forest parameters.
RF_NTREES = 500;
RF_NODESIZE = 20;

# Print Random Forest Parameters.
cat("            Number of Trees:", RF_NTREES, "\n");
cat("Minimum # of Terminal Nodes:", RF_NODESIZE, "\n");


# Get data into tables: x_train, y_train, x_test
members <- readJSON(filename);
table_list <- getTables(members);


rf_model <- randomForest(
    x = table_list[[1]],
    y = log1p(table_list[[2]]),
    ntree = RF_NTREES,
    nodesize = RF_NODESIZE,
    do.trace = TRUE
    );

# list variable importance
var_imp <- importance(rf_model);
var_imp_table <- cbind(VAR_NAMES, var_imp);
ordered_imp_table <- var_imp_table[rev(order(var_imp)),];
print(ordered_imp_table);

# predict for the leaderboard data.
prediction <- predict(rf_model, table_list[[3]]);
# Scale and cap predictions.
prediction <- transformPrediction(prediction);
summary(prediction);

# plot the submission distribution.
hist(prediction, breaks = RF_NTREES);


# Write the submission file.
origin_mids <- getColumn(members, "MemberID");
writeSubmission(prediction, origin_mids, "rf");
