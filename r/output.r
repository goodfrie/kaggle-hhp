#! /usr/bin/env Rscript

# output.r
# INFO 290 PROJECT
# Scott Goodfriend - April 20, 2012

source('../r/input.r');

output_dir <- '../../submissions/';
out_prefix <- '../../targets/';
out_suffix <- '.csv';

target_filename <- '../../HHP_release3/Target.csv';


transformPrediction <- function(prediction) {
    prediction <- expm1(prediction);
    prediction <- pmin(15, prediction);
    prediction <- pmax(0, prediction);
    prediction;
}


matchTargets <- function(preds, origin_mids, target_mids) {
    target_preds <- array(0, c(length(target_mids), 1));
    for (i in 1:length(origin_mids)) {
        target_preds[which(target_mids == origin_mids[i])] = preds[i];
    }
    target_preds;
}


getTimeStamp <- function() {
    date_time <- Sys.time();
    time_str <- sub(":[0-9]*", "",
                    sub(":", "", 
                        sub(" ", "_", date_time)));
    time_str;
}


writeSubmission <- function(prediction, origin_mids, method_name) {
    # Map target MemberIDs to the predictions.
    target_mids <- getFromCSV(target_filename, 'MemberID');
    target_cTruncated <- getFromCSV(target_filename, 'ClaimsTruncated');
    target_pred <- matchTargets(prediction, origin_mids, target_mids);

    # Create the submission table.
    submission <- cbind(
        target_mids,
        target_cTruncated,
        target_pred);
    colnames(submission) <- c("MemberID", "ClaimsTruncated", "DaysInHospital");

    # Calculate submission filename.
    infix <- getTimeStamp();
    out_filename <- paste(out_prefix, method_name, "_", infix, out_suffix,
                            sep = "");

    # Write the submission file.
    write.csv(submission, file = out_filename, row.names = FALSE);
    print(paste("Submission written to:", out_filename));
}


writeLine <- function(time_str, method_name, line) {
    filename <- paste(output_dir, method_name, "_", time_str, ".txt", 
                        sep = "");
    write(line, file = filename, append = TRUE, sep = "\r");
}
