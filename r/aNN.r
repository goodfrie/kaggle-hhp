#! /usr/bin/env Rscript

# aNN.r (artificial Neural Networks)
# INFO 290 Project
# Scott Goodfriend - April 26, 2012

source('misc.r');
source('input.r');
source('output.r');

install.packages('neuralnet');
library('neuralnet');

# Select row building methodology.
row_builder <- 'pcg_short.r';
source(row_builder);
# Select data set.
filename <- '../../data.json';


# Neural Network parameters.
NN_LEARNING_RATE = 0.007;
NN_ITERATIONS = 1e05;
NN_HIDDEN_NEURONS = c(length(VAR_NAMES), ceiling(log2(length(VAR_NAMES))));
NN_NUM_MODELS = 1;


time_str <- getTimeStamp();
writeLine(time_str, "ann",
    paste("Artificial Neural Network (aNN) using", row_builder, "from",
            filename, "at", time_str));
writeLine(time_str, "ann",
    paste("      Learning Rate:", NN_LEARNING_RATE));
writeLine(time_str, "ann",
    paste("         Iterations:", NN_ITERATIONS));
writeLine(time_str, "ann",
    paste("     Hidden Neurons:", NN_HIDDEN_NEURONS));
writeLine(time_str, "ann",
    paste("Number of Ensembles:", NN_NUM_MODELS));


# Get data into tables: x_train, y_train, x_test
members <- readJSON(filename);
table_list <- getTables(members);

# Have to convert to a table with column names for neuralnet to work.
data_table <- getNamedTable(    cbind(log1p(table_list[[2]]), table_list[[1]]),
                                c("DaysInHospital", VAR_NAMES));
nn_formula <- createFormula("DaysInHospital", VAR_NAMES);

nn_model <- neuralnet(
    nn_formula,
    data = data_table,
    learningrate = NN_LEARNING_RATE,
    stepmax = NN_ITERATIONS,
    hidden = NN_HIDDEN_NEURONS,
    rep = NN_NUM_MODELS,
    lifesign = 'full',
    lifesign.step = ceiling(NN_ITERATIONS / 100),
    );


# Compute the test inputs.
preds <- list();
for (i in 1:NN_NUM_MODELS) {
    preds[[i]] <- compute(nn_model, table_list[[3]], rep = i)$net.result;
}
pred_table <- matrix(unlist(preds), nc = NN_NUM_MODELS);
prediction <- apply(pred_table, 1, median);


# Scale and cap predictions.
prediction <- transformPrediction(prediction);
summary(prediction);

# plot the submission distribution.
hist(prediction);

# Write the submission file.
origin_mids <- getColumn(members, "MemberID");
writeSubmission(prediction, origin_mids, "ann");
