#! /usr/bin/env Rscript

# gbm.r
# INFO 290 Project
# Scott Goodfriend - April 20, 2012

options("repos" = "http://cran.us.r-project.org")
# install.packages('gbm');
library('gbm');

source('input.r');
source('output.r');
source('pcg_short.r');

# Parameters
filename <- '../../short.json';


# GBD Model settings.
GBM_NTREES = 8000;
GBM_SHRINKAGE = 0.002;
GBM_DEPTH = 7;
GBM_MINOBS = 100;
GBM_BAG_FRACTION = 0.9;
GBM_TRAIN_FRACTION = 1.0;


# Print Gradient Boosting Machine parameters:
cat(paste("  Number of Trees:", GBM_NTREES, "\n"));
cat(paste("Interaction Depth:", GBM_DEPTH, "\n"));
cat(paste("        Shrinkage:", GBM_SHRINKAGE, "\n"));
cat(paste(" Min Observations:", GBM_MINOBS, "\n"));
cat(paste("     Bag Fraction:", GBM_BAG_FRACTION, "\n"));
cat(paste("   Train Fraction:", GBM_TRAIN_FRACTION, "\n"));


members <- readJSON(filename);
table_list <- getTables(members);

gbm_model <- gbm.fit(
    x = table_list[[1]],
    y = log1p(table_list[[2]]),
    distribution = "gaussian",
    n.trees = GBM_NTREES,
    shrinkage = GBM_SHRINKAGE,
    interaction.depth = GBM_DEPTH,
    n.minobsinnode = GBM_MINOBS,
    bag.fraction = GBM_BAG_FRACTION,
    train.fraction = GBM_TRAIN_FRACTION,
    verbose = TRUE,
    var.names = VAR_NAMES
    );
warnings();


# list variable importance
summary(gbm_model, GBM_NTREES);


# predict for the leaderboard data.
prediction <- predict.gbm(object = gbm_model,
    newdata = table_list[[3]],
    GBM_NTREES);


# put on correct scale and cap.
prediction <- transformPrediction(prediction);
summary(prediction);

# plot the submission distribution.
hist(prediction, breaks = GBM_NTREES);


# Write the submission file.
origin_mids <- getColumn(members, "MemberID");
writeSubmission(prediction, origin_mids, "gbm");


# Using short_info.r; 100, 0.05, 4, 20: 0.471326 (391/1006)
#                var    rel.inf
#                1          # Claims 63.3378596
#                2 Max CharlsonIndex 13.1989484
#                3   ClaimsTruncated 10.4961346
#                4   AgeAtFirstClaim  7.2771228
#                5               Sex  3.3990176
#                6           # Drugs  1.5170378
#                7            # Labs  0.7738792


# Using pcg_short.r; 500, 0.05, 4, 50: 
#                  var      rel.inf
# 1           # Claims 43.705803235
# 2  Max CharlsonIndex  8.777870103
# 3                Sex  7.489719866
# 4               <NA>  6.998872590
# 5         PCG-PRGNCY  5.899807260
# 6    AgeAtFirstClaim  4.785362032
# 7            PCG-AMI  2.113002189
# 8          PCG-ROAMI  1.853279279
# 9         PCG-RENAL2  1.535112680
# 10       PCG-GIBLEED  1.516392791
# 11            # Labs  1.348144635
# 12   ClaimsTruncated  1.284500413
# 13        PCG-MSC2a3  1.224422743
# 14           PCG-CHF  0.898178316
# 15        PCG-HEMTOL  0.834910229
# 16      PCG-ARTHSPIN  0.744389965
# 17        PCG-METAB3  0.682620228
# 18       PCG-SEIZURE  0.642529195
# 19          PCG-COPD  0.596422292
# 20        PCG-HEART2  0.551119386
# 21        PCG-SKNAUT  0.473533797
# 22       PCG-NEUMENT  0.402565960
# 23       PCG-ODaBNCA  0.392803635
# 24        PCG-RESPR4  0.387525198
# 25           # Drugs  0.342957128
# 26        PCG-TRAUMA  0.294768524
# 27        PCG-INFEC4  0.293264587
# 28        PCG-RENAL3  0.286134738
# 29       PCG-MISCHRT  0.284802305
# 30        PCG-CANCRB  0.281463513
# 31        PCG-GYNEC1  0.274375747
# 32           PCG-UTI  0.257950043
# 33       PCG-FXDISLC  0.240936859
# 34        PCG-MISCL5  0.228510569
# 35       PCG-APPCHOL  0.226393765
# 36        PCG-CANCRA  0.220695974
# 37        PCG-HEART4  0.199646006
# 38       PCG-FLaELEC  0.195575490
# 39        PCG-STROKE  0.191475677
# 40         PCG-HIPFX  0.178460745
# 41         PCG-PNEUM  0.127276995
# 42        PCG-METAB1  0.121758424
# 43       PCG-PERVALV  0.118385204
# 44      PCG-GIOBSENT  0.113165899
# 45        PCG-CATAST  0.093141697
# 46        PCG-GYNECA  0.078357936
# 47        PCG-MISCL1  0.075643231
# 48       PCG-LIVERDZ  0.075398236
# 49       PCG-PERINTL  0.025292563
# 50        PCG-RENAL1  0.018946696
# 51        PCG-PNCRDZ  0.008624726
# 52        PCG-SEPSIS  0.007708710
# 53        PCG-CANCRM  0.000000000
#     Min.      1st Qu.     Median      Mean        3rd Qu.     Max. 
#     0.000000  0.002716    0.077760    0.136300    0.176500    2.947000 
# [1] "Submission written to: ../../targets_gbm_ 2012-04-21_1145 .csv"
# 
#     real    38m47.879s
#     user    28m58.982s
#     sys     8m35.104s
