#! /usr/bin/env Rscript

# short_infos.r
# INFO 290 Project
# Scott Goodfriend - April 20, 2012

VAR_NAMES = c(
    "Sex",
    "AgeAtFirstClaim",
    "# Claims",
    "Max CharlsonIndex",
    "# Drugs",
    "# Labs",
    "ClaimsTruncated");

getClaimInfos <- function(claims) {
    years <- getColumn(claims, 'Year');
    charlson_indexes <- getColumn(claims, 'CharlsonIndex');
    
    claims_infos <- list();
    for (i in 1:3) {
        indexes <- years == i;
        
        count <- sum(indexes);
        if (count) {
            max_cis <- max(charlson_indexes[indexes]);
        }
        else {
            max_cis <- 0;
        }
        
        claims_infos[[i]] <- c(count, max_cis);
    }

    claims_infos;
}


getDrugInfos <- function(drugs) {
    years <- getColumn(drugs, 'Year');
    
    drugs_infos <- list();
    for (i in 1:3) {
        indexes <- years == i;
        
        count <- sum(indexes);
        
        drugs_infos[[i]] <- count;
    }

    drugs_infos;
}


getLabInfos <- function(labs) {
    years <- getColumn(labs, 'Year');

    labs_infos <- list();
    for (i in 1:3) {
        indexes <- years == i;
        
        count <- sum(indexes);

        labs_infos[[i]] <- count;
    }

    labs_infos;
}


getDIHInfos <- function(member) {
    dih_infos <- list();
    for (i in 1:3) {
        c_cmd <- sprintf("member$ClaimsTruncated_Y%i", i + 1);
        cTrunc <- eval(parse(text = c_cmd));
        if (cTrunc < 0) { 
            # Warning: This could be a disastrous assumption.
            dih_infos[[i]] <- list(0, 0);
        }
        else {
            d_cmd <- sprintf("member$DaysInHospital_Y%i", i + 1);
            dih <- eval(parse(text = d_cmd));
            dih_infos[[i]] <- list(cTrunc, dih);
        }
    }
    dih_infos;
}
