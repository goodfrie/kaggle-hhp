#! /usr/bin/env Rscript

# input.r
# INFO 290 Project
# Scott Goodfriend - April 20, 2012

# install.packages('rjson');
library('rjson');

readJSON <- function(filename) {
    file_text <- readLines(filename);

    members <- list();

    for (i in 1:length(file_text)) {
        obj <- fromJSON(file_text[i]);
        members[[i]] <- obj;
    }

    members
}


getColumn <- function(members, column_name) {
    column <- array(0, dim = c(length(members), 1));
    if (length(members)) {
        for (i in 1:length(members)) {
            cmd <- sprintf("members[[%i]]$%s", i, column_name);
            column[i] <- eval(parse(text = cmd));
        }
    }
    column
}


getRow <- function(members, row_number) {
    member <- members[[row_number]];
    m_info <- c(member$Sex, member$AgeAtFirstClaim);
    claim_infos <- getClaimInfos(member$Claims);
    drug_infos <- getDrugInfos(member$DrugCount);
    lab_infos <- getLabInfos(member$LabCount);
    dih_infos <- getDIHInfos(member);
    row <- list();
    for (i in 1:3) {
        row[[i * 2 - 1]] <- c(  m_info,
                                claim_infos[[i]],
                                drug_infos[[i]],
                                lab_infos[[i]],
                                dih_infos[[i]][[1]]);
        row[[i * 2]] <- dih_infos[[i]][[2]];
    }
    row;
}


getTables <- function(members) {
    training_x_list <- list();
    training_y_list <- list();
    test_x_list <- list();
    for (i in 1:length(members)) {
        member <- getRow(members, i);

        training_x_list[[i * 2 - 1]] <- member[[1]];
        training_y_list[[i * 2 - 1]] <- member[[2]];

        training_x_list[[i * 2]] <- member[[3]];
        training_y_list[[i * 2]] <- member[[4]];

        test_x_list[[i]] <- member[[5]];
    }

    x_training <- t(do.call(cbind, training_x_list));
    y_training <- unlist(training_y_list);

    x_test <- t(do.call(cbind, test_x_list));

    list(x_training, y_training, x_test);
}


getFromCSV <- function(target_filename, column) {
    data <- read.csv(target_filename);
    cmd <- sprintf("data$%s", column);
    member_ids <- eval(parse(text = cmd));
}


getTargets <- function(target_filename) {
    targets <- read.csv(target_filename);
    member_ids <- targets$MemberID;
}
