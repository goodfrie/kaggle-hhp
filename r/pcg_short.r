#! /usr/bin/env Rscript

# pcg_short.r - Modified from short_info.r (Apr 20, 2012)
# Abbreviated Columns Dataset
# INFO 290 Project
# Scott Goodfriend - April 21, 2012

source('misc.r');

VAR_NAMES = c(
    "Sex",
    "AgeAtFirstClaim",
    "NumClaims",
    "MaxCharlsonIndex",
    "PCG_AMI",
    "PCG_APPCHOL",
    "PCG_ARTHSPIN",
    "PCG_CANCRA",
    "PCG_CANCRB",
    "PCG_CANCRM",
    "PCG_CATAST",
    "PCG_CHF",
    "PCG_COPD",
    "PCG_FLaELEC",
    "PCG_FXDISLC",
    "PCG_GIBLEED",
    "PCG_GIOBSENT",
    "PCG_GYNEC1",
    "PCG_GYNECA",
    "PCG_HEART2",
    "PCG_HEART4",
    "PCG_HEMTOL",
    "PCG_HIPFX",
    "PCG_INFEC4",
    "PCG_LIVERDZ",
    "PCG_METAB1",
    "PCG_METAB3",
    "PCG_MISCHRT",
    "PCG_MISCL1",
    "PCG_MISCL5",
    "PCG_MSC2a3",
    "PCG_NEUMENT",
    "PCG_ODaBNCA",
    "PCG_PERINTL",
    "PCG_PERVALV",
    "PCG_PNCRDZ",
    "PCG_PNEUM",
    "PCG_PRGNCY",
    "PCG_RENAL1",
    "PCG_RENAL2",
    "PCG_RENAL3",
    "PCG_RESPR4",
    "PCG_ROAMI",
    "PCG_SEIZURE",
    "PCG_SEPSIS",
    "PCG_SKNAUT",
    "PCG_STROKE",
    "PCG_TRAUMA",
    "PCG_UTI",
    "PCG_NA",
    "NumDrugs",
    "NumLabs",
    "ClaimsTruncated");

getClaimInfos <- function(claims) {
    years <- getColumn(claims, 'Year');
    charlson_indexes <- getColumn(claims, 'CharlsonIndex');
    pcg_indexes <- getColumn(claims, 'PrimaryConditionGroup');
    
    claims_infos <- list();
    for (i in 1:3) {
        indexes <- years == i;
        pcgs <- vector(mode = "integer", length = 46);
        
        count <- sum(indexes);
        if (count) {
            max_cis <- max(charlson_indexes[indexes]);
        }
        else {
            max_cis <- 0;
        }
        
        claims_infos[[i]] <- c(count, max_cis, pcgs);
    }

    pcg_offset = 3;
    # Increment through every claim.
    if (length(pcg_indexes)) {
        for (i in 1:length(pcg_indexes)) {
            increment(claims_infos[[ years[i] ]][ pcg_indexes[i] + pcg_offset ])
        }
    }

    claims_infos;
}


getDrugInfos <- function(drugs) {
    years <- getColumn(drugs, 'Year');
    
    drugs_infos <- list();
    for (i in 1:3) {
        indexes <- years == i;
        
        count <- sum(indexes);
        
        drugs_infos[[i]] <- count;
    }

    drugs_infos;
}


getLabInfos <- function(labs) {
    years <- getColumn(labs, 'Year');

    labs_infos <- list();
    for (i in 1:3) {
        indexes <- years == i;
        
        count <- sum(indexes);

        labs_infos[[i]] <- count;
    }

    labs_infos;
}


getDIHInfos <- function(member) {
    dih_infos <- list();
    for (i in 1:3) {
        c_cmd <- sprintf("member$ClaimsTruncated_Y%i", i + 1);
        cTrunc <- eval(parse(text = c_cmd));
        if (cTrunc < 0) { 
            # Warning: This could be a disastrous assumption.
            dih_infos[[i]] <- list(0, 0);
        }
        else {
            d_cmd <- sprintf("member$DaysInHospital_Y%i", i + 1);
            dih <- eval(parse(text = d_cmd));
            dih_infos[[i]] <- list(cTrunc, dih);
        }
    }
    dih_infos;
}
