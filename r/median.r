#! /usr/bin/env Rscript

# median.r
# INFO 290 Project
# Scott Goodfriend - April 27, 2012

source('misc.r');
source('input.r');
source('output.r');


target_dir = '../../targets/submitted/';
SUB_NAMES = c(
    'gbm_2012-04-21_1725.csv',
    'rf_2012-04-23_1309.csv',
    'gbm_2012-04-24_0208.csv',
    'rf_2012-04-25_2210.csv');

# Get all of the DaysInHospital predictions.
predictions = list();
for (i in 1:length(SUB_NAMES)) {
    filename <- paste(target_dir, SUB_NAMES[i], sep = "");
    predictions[[i]] = getFromCSV(filename, "DaysInHospital");
}

# Find median for each prediction.
data_table <- matrix(unlist(predictions), nc = 4);
medians <- apply(data_table, 1, median);

# Summary and histogram.
summary(medians);
hist(medians);

# Write the submission file.
origin_mids <- getFromCSV(filename, "MemberID");
writeSubmission(medians, origin_mids, "median");
