# ref_claimNames.r
# INFO 290 Project
# Scott Goodfriend - May 9, 2012

place_list = c(
        "Ambl", # "Ambulance",            #  34766
        "Home", # "Home",                 # 8303
        "iLab", # "Independent Lab",      # 657750
        "iHos", # "Inpatient Hospital",   # 85776
        "Offc", # "Office",               # 1542007
        "oHos", # "Outpatient Hospital",  # 121528
        "urgC", # "Urgent Care",          # 199528
        "Othr", # "Other",                # 11700
        "?"    # "",                     # 7632
        );
placeSvc <- function() {
    place_service <- paste("PlSvc_", place_list, sep = "");
    place_service;
}


specialties = c(
        "Anesth",   # "Anesthesiology"            : 0,    # 33435
        "DigImg",   # "Diagnostic Imaging"        : 1,    # 207297
        "Emergn",   # "Emergency"                 : 2,    # 126130
        "GenPra",   # "General Practice"          : 3,    # 473655
        "Intrnl",   # "Internal"                  : 4,    # 672059
        "Labrty",   # "Laboratory"                : 5,    # 653188
        "ObstGy",   # "Obstetrics and Gynecology" : 6,    # 36594
        "Pathol",   # "Pathology"                 : 7,    # 14907
        "Pediat",   # "Pediatrics"                : 8,    # 84862
        "Rehabl",   # "Rehabilitation"            : 9,    # 57554
        "Surgry",   # "Surgery"                   : 10,   # 208217
        "Other",    #                             : 11,   # 92687
        "?"        # ""                          : 12,   # 8405
        );
specialtyNames <- function() {
    spec_names <- paste("special_", specialties, sep = "");
    spec_names;
}


pcgs = c(
        "AMI",                  # 34805     1
        "APPCHOL",		# 17945     2
        "ARTHSPIN",             # 288285    3
        "CANCRA",		# 5587      4
        "CANCRB",		# 42895     5
        "CANCRM",		# 1096      6
        "CATAST",		# 2070      7   
        "CHF",		        # 13316     8
        "COPD",                 # 44154     9
        "FLaELEC",		# 5263      10
        "FXDISLC",		# 40851     11
        "GIBLEED",		# 101846    12
        "GIOBSENT",             # 9718      13
        "GYNEC1",		# 44143     14
        "GYNECA",		# 12491     15
        "HEART2",		# 54207     16
        "HEART4",		# 28733     17
        "HEMTOL",		# 31631     18
        "HIPFX" ,               # 4355      19
        "INFEC4",		# 83552     20
        "LIVERDZ",		# 2747      21
        "METAB1",		# 3863      22
        "METAB3",		# 320553    23
        "MISCHRT",		# 131047    24
        "MISCL1",		# 4892      25
        "MISCL5",		# 48307     26
        "MSC2a3",		# 507277    27
        "NEUMENT",		# 171605    28
        "ODaBNCA",		# 46732     29
        "PERINTL",		# 980       30
        "PERVALV",		# 3518      31
        "PNCRDZ",		# 912       32
        "PNEUM",                # 11333     33
        "PRGNCY",		# 32004     34
        "RENAL1",		# 602       35
        "RENAL2",		# 10922     36
        "RENAL3",		# 52214     37
        "RESPR4",		# 138062    38
        "ROAMI",                # 48821     39
        "SEIZURE",		# 20501     40
        "SEPSIS",		# 497       41
        "SKNAUT",		# 107976    42
        "STROKE",		# 8416      43
        "TRAUMA",		# 72050     44
        "UTI",		        # 44806     45
        ""                     # 11410     46
        );
primaryConditionGroups <- function() {
    pcg_names <- paste("PCG_", pcgs, sep = "");
    pcg_names;
}


proc_list <- c(
        "ANES", # 17061
        "EM",   # 1048210
        "MED",  # 372101
        "PL",   # 492919
        "RAD",  # 265272
        "SAS",  # 5745
        "SCS",  # 274805
        "SDS",  # 60678
        "SEOA", # 8420
        "SGS",  # 9406
        "SIS",  # 56461
        "SMCD", # 3376
        "SMS",  # 29177
        "SNS",  # 7796
        "SO",   # 371
        "SRS",  # 7905
        "SUS",  # 5612
        ""     # 3675
    );
procedureList <- function() {
    proc_names <- paste("PROCEDURE_", proc_list, sep = "");
    proc_names;
}
