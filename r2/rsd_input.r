#! /usr/bin/env Rscript

# rsd_input.r
# Extended Columns dataset.
# INFO 290 Project
# Scott Goodfriend - May 9, 2012


# "imports"
# ---------------------------------------------------------------------------
source('../r/misc.r');

source('ref_claimNames.r');



# gen_info <- getGeneralInfo(member)
# ---------------------------------------------------------------------------
ageNames <- function() {
    age_names <- NULL;
    for (i in 0:8) {
        age_names <- c(age_names, paste("Age_", i, "0s", sep = ""));
    }
    age_names;
}


GI_VAR_NAMES <- c(
    "Sex",
    ageNames());


getGeneralInfo <- function(member) {
    gen_info <- array(0, dim = c(1, length(GI_VAR_NAMES)));
    gen_info[1] <- member$Sex;

    age <- member$AgeAtFirstClaim;
    if (age %% 10) {
        index <- floor(age / 10);
        increment(gen_info[index + 2]);
    }

    colnames(gen_info) <- GI_VAR_NAMES;
    gen_info;
}



# claim_infos <- getClaimInfos(claims)
# ---------------------------------------------------------------------------
CL_VAR_NAMES = c(
    "NumClaims",
    "MaxDSFS",
    "MaxCharlsonIndexAtLastDSFS",
    "MaxCharlsonIndex",
    "MaxLOS",
    "TotalLOS",
    placeSvc(),
    specialtyNames(),
    primaryConditionGroups(),
    procedureList());
getClaimInfos <- function(claims) {
    gen_claims <- array(0, dim = c(3, 6));
    p_svc <- array(0, dim = c(3, length(placeSvc())));
    s_names <- array(0, dim = c(3, length(specialtyNames())));
    pcgs <- array(0, dim = c(3, length(primaryConditionGroups())));
    p_list <- array(0, dim = c(3, length(procedureList())));
    
    for (i in 1:length(claims)) {
        claim <- claims[[i]];
        year <- claim$Year;

        # NumClaims
        increment(gen_claims[year, 1]);

        # MaxDSFS, MaxCharlsonIndexAtLastDSFS, MaxCharlsonIndex
        dsfs <- claim$DSFS;
        charlson_index <- claim$CharlsonIndex;
        if (dsfs > gen_claims[year, 2]) {
            gen_claims[year, 2] <- dsfs;
            gen_claims[year, 3] <- charlson_index;
        }
        else {
            if (dsfs == gen_claims[year, 2] 
                && charlson_index > gen_claims[year, 3]) {
                gen_claims[year, 3] <- charlson_index;
            }
        }

        if (charlson_index > gen_claims[year, 4]) {
            gen_claims[year, 4] <- charlson_index;
        }

        # MaxLOS
        length_of_stay <- claim$LengthOfStay;
        if (length_of_stay > gen_claims[year, 5]) {
            gen_claims[year, 5] <- length_of_stay;
        }

        # TotalLOS
        increment(gen_claims[year, 6], length_of_stay);

        # PlaceSvc
        place_service <- claim$PlaceSvc;
        increment(p_svc[place_service + 1]);

        # Specialty
        specialty <- claim$Specialty;
        increment(s_names[specialty + 1]);

        # PCG
        primary_condition_group <- claim$PrimaryConditionGroup;
        increment(pcgs[primary_condition_group + 1]);

        # ProcedureGroup
        procedure <- claim$ProcedureGroup
        increment(p_list[procedure + 1]);
    }

    claim_infos <- cbind(gen_claims, p_svc, s_names, pcgs, p_list);
    colnames(claim_infos) <- CL_VAR_NAMES;

    claim_infos;
}



# drug_infos <- getDrugInfos(drug_counts)
# ---------------------------------------------------------------------------
DR_VAR_NAMES <- c(
    "NumDrugs",
    "MaxDrugDSFS",
    "MaxDrugCountAtMaxDSFS",
    "MaxDrugCount",
    "TotalDrugCount");
getDrugInfos <- function(drug_counts) {
    drug_infos <- array(0, dim = c(3, 5));
    colnames(drug_infos) <- DR_VAR_NAMES;

    for (i in loop(length(drug_counts))) {
        drug <- drug_counts[[i]];

        dsfs <- drug$DSFS;
        drug_count <- drug$DrugCount;
        year <- drug$Year;

        # NumDrugs
        increment(drug_infos[year, 1]);

        # MaxDSFS, MaxDrugCountAtMaxDSFS
        if (dsfs > drug_infos[year, 2]) {
            drug_infos[year, 2] <- dsfs;
            drug_infos[year, 3] <- drug_count;
        }
        else {
            if (dsfs == drug_infos[year, 2]
                && drug_count > drug_infos[year, 3]) {
                drug_infos[year, 3] <- drug_count;
            }
        }

        # MaxDrugCount
        if (drug_count > drug_infos[year, 4]) {
            drug_infos[year, 4] <- drug_count;
        }

        # TotalDrugCount
        increment(drug_infos[year, 5], drug_count);
    }

    drug_infos;
}



# lab_infos <- getLabInfos(lab_counts)
# ---------------------------------------------------------------------------
LB_VAR_NAMES <- c(
    "NumLabs",
    "MaxLabDSFS",
    "MaxLabCountAtMaxDSFS",
    "MaxLabCount",
    "TotalLabCount");
getLabInfos <- function(lab_counts) {
    lab_infos <- array(0, dim = c(3, length(LB_VAR_NAMES)));
    colnames(lab_infos) <- LB_VAR_NAMES;

    for (i in loop(length(lab_counts))) {
        lab <- lab_counts[[i]];

        dsfs <- lab$DSFS;
        lab_count <- lab$LabCount;
        year <- lab$Year;

        # NumLabs
        increment(lab_infos[year, 1]);

        # MaxLabDSFS, MaxLabCountAtMaxDSFS.
        if (dsfs > lab_infos[year, 2]) {
            lab_infos[year, 2] <- dsfs;
            lab_infos[year, 3] <- lab_count;
        }
        else {
            if (dsfs == lab_infos[year, 2]
                && lab_count > lab_infos[year, 3]) {
                lab_infos[year, 3] <- lab_count;
            }
        }

        # MaxLabCount
        if (lab_count > lab_infos[year, 4]) {
            lab_infos[year, 4] <- lab_count;
        }

        # TotalLabCount
        increment(lab_infos[year, 5], lab_count);
    }

    lab_infos;
}

    

# dih_infos <- getDIHInfos(member)
# ---------------------------------------------------------------------------
DH_VAR_NAMES <- c(
    "ClaimsTruncated",
    "DaysInHospital");
getDIHInfos <- function(member) {
    dih_infos <- list(NULL, NULL, NULL);
    for (i in 1:3) {
        c_cmd <- sprintf("member$ClaimsTruncated_Y%i", i + 1);
        cTrunc <- eval(parse(text = c_cmd));
        if (cTrunc >= 0) {
            d_cmd <- sprintf("member$DaysInHospital_Y%i", i + 1);
            dih <- eval(parse(text = d_cmd));
            dih_infos[[i]] <- list( ClaimsTruncated = cTrunc,
                                    DaysInHospital = dih);
        }
    }

    dih_infos;
}



# ---------------------------------------------------------------------------
# m_rsd <- memberToRSD(member)
#       [=] list(info = list(NULL, NULL, NULL), dihs = list(NULL, NULL, NULL));
# ---------------------------------------------------------------------------
VAR_NAMES <- c(
    GI_VAR_NAMES,
    CL_VAR_NAMES,
    DR_VAR_NAMES,
    LB_VAR_NAMES,
    DH_VAR_NAMES[1]
    );
memberToRSD <- function(member) {
    m_rsd <- list(  info = list(NULL, NULL, NULL),
                    dihs = list(NULL, NULL, NULL));

    gen_info <- getGeneralInfo(member);

    claim_infos <- getClaimInfos(member$Claims);
    drug_infos <- getDrugInfos(member$DrugCount);
    lab_infos <- getLabInfos(member$LabCount);

    dih_infos <- getDIHInfos(member);
    for (i in 1:3) {
        if (!is.null(dih_infos[[i]])) {
            m_rsd$info[[i]] <- c(m_rsd$info[[i]],
                                    list(c(gen_info,
                                        claim_infos[i, ],
                                        drug_infos[i, ],
                                        lab_infos[i, ],
                                        dih_infos[[i]]$ClaimsTruncated)));
            if (i < 3) {
                m_rsd$dihs[[i]] <- c(   m_rsd$dihs[[i]],
                                        dih_infos[[i]]$DaysInHospital);
            }
        }
    }

    m_rsd;
}
