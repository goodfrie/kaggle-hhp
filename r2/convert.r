#! /usr/bin/env Rscript

# convert.r
# INFO 290 Project
# Scott Goodfriend - May 9, 2012


# "Imports"
# ---------------------------------------------------------------------------
# install.packages('rjson');
library('rjson');

source('dim_proc.r');
source('estimate_age.r');
source('estimate_sex.r');
source('rsd_input.r');


# Parameters
# ---------------------------------------------------------------------------
data_filename <- '../../data.json';
orig_dir <- '../../r_orig/';
out_dir <- '../../R_data/';
# ---------------------------------------------------------------------------


# members <- readJSON(filename)
# ---------------------------------------------------------------------------
readJSON <- function(filename) {
    file_text <- readLines(filename);
    
    members <- list();

    for (i in 1:length(file_text)) {
        obj <- fromJSON(file_text[i]);
        members[[i]] <- obj;
    }

    members;
}


# rsd <- convertMembersToRSD(members)
# ---------------------------------------------------------------------------
convertMembersToRSD <- function(members) {
    info <- list(list(), list(), list());
    m_ids <- list(c(), c(), c());
    dihs <- list(list(), list(), list());

    for (i in 1:length(members)) {
        m <- members[[i]];
        member <- memberToRSD(m);
        m_id <- m$MemberID;

        for (j in 1:3) {
            if (!is.null(member$info[[j]])) {
                info[[j]] <- c(info[[j]], member$info[[j]]);
                m_ids[[j]] <- rbind(m_ids[[j]], 
                    c(m_id, member$info[[j]][[1]][which(
                                        VAR_NAMES == "ClaimsTruncated")]));
            }
            if (!is.null(member$dihs[[j]])) {
                dihs[[j]] <- c(dihs[[j]], member$dihs[[j]]);
            }
        }

        if (i %% 1000 == 0) {
            cat(sprintf("  RSD Converstion completed %d.\n", i));
        }
    }

    for (i in 1:3) {
        info[[i]] <- t(do.call(cbind, info[[i]]));
        colnames(info[[i]]) <- VAR_NAMES;
        colnames(m_ids[[i]]) <- c("MemberID", "ClaimsTruncated");
        dihs[[i]] <- unlist(dihs[[i]]);
    }
    
    rsd <- list(info=info, m_ids = m_ids, dihs=dihs[1:2]);
    rsd;
}


# rsd <- datasetLNC(rsd)
# ---------------------------------------------------------------------------
datasetLNC <- function(rsd) {
    train_x <- NULL;
    indexes <- NULL;
    for (i in 1:length(rsd$info)) {
        train_x <- rbind(train_x, rsd$info[[i]]);
        indexes <- c(indexes, dim(rsd$info[[i]])[1]);
    }
    train_x <- lNC_nAS(train_x);

    stops <- c(1);
    for (i in 1:length(rsd$info)) {
        stops <- c(stops, indexes[i] + stops[i]);
    }

    for (i in 1:length(rsd$info)) {
        rsd$info[[i]] <- train_x[stops[i]:(stops[i+1] - 1), ];
    }

    rsd;
}


# NULL <- writeRSD(rsd)
# ---------------------------------------------------------------------------
writeRSD <- function(rsd, dir_name) {
    for (i in 1:3) {
        out_filename <- paste(dir_name, "info_Y", i, ".csv", sep = "");
        write.csv(rsd$info[[i]], file = out_filename, row.names = FALSE);
    }
    for (i in 1:3) {
        out_filename <- paste(dir_name, "m_ids_Y", i, ".csv", sep = "");
        write.csv(rsd$m_ids[[i]], file = out_filename, row.names = FALSE);
    }
    for (i in 1:2) {
        out_filename <- paste(dir_name, "dihs_Y", i+1, ".csv", sep = "");
        write.csv(rsd$dihs[[i]], file = out_filename, row.names = FALSE);
    }
}



# ---------------------------------------------------------------------------
# convert.r execution
# ---------------------------------------------------------------------------
cat("Beginning conversion of JSON data...\n");
# members [=] list of nested tables.
members <- readJSON(data_filename);
cat("Member loading complete.\n");

# rsd [=] {info, dihs} [=] {{info_Y1, info_Y2, info_Y3}, {dihs_Y2, dihs_Y3}}
#   where info_Y# is a table matched to associated dihs_Y#.
rsd <- convertMembersToRSD(members);
cat("Conversion to RSD format complete.\n");

# Write this to file (because it takes forever).
writeRSD(rsd, orig_dir);
cat("Original RSD written.\n");

# Estimate missing Sex and Age.
rsd <- estimateSex(rsd);
cat("Sex estimation complete.\n");
rsd <- estimateAge(rsd);
cat("Age estimation complete.\n");

# Run logNormalizeColumns on the entire dataset.
rsd <- datasetLNC(rsd);

# Write rsd into 5 csv files.
writeRSD(rsd, out_dir);
cat("File writing complete.\n");
