# dim_proc.r
# INFO 290 Project
# Scott Goodfriend - May 10, 2012

source('../r/misc.r');


# squashed_col <- squashColumn(col)
# ---------------------------------------------------------------------------
squashColumn <- function(col) {
    width <- diff(range(col));
    if (width > 0) {
        col <- (col - min(col)) / width;
    }

    col;
}



# logN_col <- logNormalizeColumns(info, log_cols)
# ---------------------------------------------------------------------------
logNormalizeColumns <- function(info, log_cols) {
    info[, log_cols] <- log1p(info[, log_cols]);
    
    logN_col <- apply(info, 2, squashColumn);

    logN_col;
}

lNC_nAS <- function(info) {
    logN_col <- logNormalizeColumns(info, 
                    VAR_NAMES[-grep("Age|Sex", VAR_NAMES)]);
}



# pc_load <- findPrincipalComponentLoading(info, min_var)
# ---------------------------------------------------------------------------
findPrincipalComponentLoading <- function(info, min_var) {
    pc <- princomp(info);

    pc_var <- pc$sdev**2 / sum(pc$sdev**2);
    dims <- 0;
    cum_var <- 0;
    while (cum_var < min_var) {
        increment(dims);
        increment(cum_var, pc_var[dims]);
    }

    pc_load <- pc$loadings[,1:dims];
}
