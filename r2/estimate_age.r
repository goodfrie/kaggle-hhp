# estimate_age.r
# INFO 290 Project
# Scott Goodfriend - May 10, 2012

# "imports"
# ---------------------------------------------------------------------------
options("repos" = "http://cran.us.r-project.org");
# install.packages('gbm');
library('gbm');

source('../r/misc.r');

source('dim_proc.r');


# Parameters
# ---------------------------------------------------------------------------
EA_NTREES = 3000;
EA_SHRINKAGE = 0.005;
EA_DEPTH = 5;
EA_MINOBS = 50;
EA_BAG_FRACTION = 0.95;
EA_TRAIN_FRACTION = 1.0;



# ---------------------------------------------------------------------------
# rsd <- estimateAge(rsd)
# ---------------------------------------------------------------------------
estimateAge <- function(rsd) {
    train_x <- NULL;
    train_y <- NULL;
    for (i in loop(length(rsd$info))) {
        info <- rsd$info[[i]];
        ilnc <- lNC_nAS(info);

        y_all <- ilnc[, ageNames()];
        x_all <- ilnc[, -grep("Age", VAR_NAMES)];

        train_rows <- which(rowSums(y_all) != 0);
        
        train_x <- rbind(train_x,    x_all[train_rows, ]);
        train_y <- rbind(train_y,    y_all[train_rows, ]);
    }

    pc_load <- findPrincipalComponentLoading(train_x, 0.925);
    cat(sprintf("Estimated Age using %d dimentions.\n", dim(pc_load)[2]));
    train_x <- train_x %*% pc_load;

    gbm_models <- list(array(list(), dim = c(9, 1)));
    for (i in 1:length(ageNames())) {
        y_col <- train_y[,i];

        gbm_model <- gbm.fit(
            x = train_x,
            y = y_col,
            distribution = "gaussian",
            n.trees = EA_NTREES,
            shrinkage = EA_SHRINKAGE,
            interaction.depth = EA_DEPTH,
            n.minobsinnode = EA_MINOBS,
            bag.fraction = EA_BAG_FRACTION,
            verbose = TRUE
            );
        warnings();
        
        # list variable importance.
        summary(gbm_model, EA_NTREES);

        gbm_models[[i]] <- gbm_model;
    }

    for (i in loop(length(rsd$info))) {
        info <- rsd$info[[i]];
        ilnc <- lNC_nAS(info);

        y_all <- ilnc[, ageNames()];
        x_all <- ilnc[, -grep("Age", VAR_NAMES)];

        test_rows <- which(rowSums(y_all) == 0);
        test_x <- x_all[test_rows, ];
        test_x <- test_x %*% pc_load;

        pred_y <- array(0, dim = c(length(test_rows), length(ageNames())));
        for (j in 1:length(gbm_models)) {
            pred_y[,j] <- predict.gbm(object = gbm_models[[j]],
                                        newdata = test_x,
                                        EA_NTREES);
        }
        pred_y <- array(pmax(0, pred_y), dim = dim(pred_y));

        pred_y <- t(apply(pred_y, 1, euclidianNormalize));

        age_col <- grep("Age", VAR_NAMES);
        rsd$info[[i]][test_rows, age_col] <- pred_y;
    }

    rsd;
}
