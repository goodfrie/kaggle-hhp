# estimate_sex.r
# INFO 290 Project
# Scott Goodfriend - May 10, 2012

# "imports"
# ---------------------------------------------------------------------------
options("repos" = "http://cran.us.r-project.org");
# install.packages('gbm');
library('gbm');

source('../r/misc.r');

source('dim_proc.r');


# Parameters
# ---------------------------------------------------------------------------
ES_NTREES = 5000;
ES_SHRINKAGE = 0.005;
ES_DEPTH = 5;
ES_MINOBS = 50;
ES_BAG_FRACTION = 0.95;
ES_TRAIN_FRACTION = 1.0;


# ---------------------------------------------------------------------------
# new_rsd <- estimateSex(rsd)
# ---------------------------------------------------------------------------
estimateSex <- function(rsd) {
    train_x <- NULL;
    train_y <- NULL;
    for (i in loop(length(rsd$info))) {
        info <- rsd$info[[i]];
        ilnc <- lNC_nAS(info);

        y_all <- info[, 'Sex'];
        x_all <- subset(ilnc, select = -Sex);

        train_rows <- which(y_all != 0);

        train_x <- rbind(   train_x,    x_all[train_rows, ]);
        train_y <- c(       train_y,    y_all[train_rows]);
    }

    pc_load <- findPrincipalComponentLoading(train_x, 0.95);
    cat(sprintf("Estimated Sex using %d dimensions.\n", dim(pc_load)[2]));
    train_x <- train_x %*% pc_load;

    gbm_model <- gbm.fit(
                x = train_x,
                y = train_y,
                distribution = "gaussian",
                n.trees = ES_NTREES,
                shrinkage = ES_SHRINKAGE,
                interaction.depth = ES_DEPTH,
                n.minobsinnode = ES_MINOBS,
                bag.fraction = ES_BAG_FRACTION,
                verbose = TRUE,
                );
    warnings();

    # list variable importance.
    summary(gbm_model, ES_NTREES);

    for (i in loop(length(rsd$info))) {
        info <- rsd$info[[i]];
        ilnc <- lNC_nAS(info);

        y_all <- info[, 'Sex'];
        x_all <- subset(ilnc, select = -Sex);

        test_rows <- which(y_all == 0);

        test_x <- x_all[test_rows, ];
        test_x <- test_x %*% pc_load;

        pred_y <- predict.gbm(object = gbm_model,
            newdata = test_x,
            ES_NTREES);

        pred_y <- pmin(1, pred_y);
        pred_y <- pmax(-1, pred_y);

        y_all[test_rows] <- pred_y;

        sex_col <- which(VAR_NAMES == 'Sex');
        rsd$info[[i]][,'Sex'] <- y_all;
    }

    rsd;
}
