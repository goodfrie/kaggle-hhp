#! /usr/bin/env Rscript

# ugbm.r (undersampled, covered gradient boosting machine)
# INFO 290 Project 
# Scott Goodfriend - May 10, 2012

# "Import"
# ---------------------------------------------------------------------------
# install.packages('gbm');
library('gbm');

source('../r/misc.r');
source('../r/output.r');

source('dim_proc.r');



# Pamameters
# ---------------------------------------------------------------------------
csv_dir <- '../../R_data/';
info_fileTemplates <- 'info_Y%d.csv';
mids_fileTemplates <- 'm_ids_Y%d.csv';
dihs_fileTemplates <- 'dihs_Y%d.csv';

gbm_parameters <- list(
    n_trees         = 3000,
    shrinkage       = 0.005,
    depth           = 5,
    min_obs         = 50,
    bag_fraction    = 0.95,
    train_fraction  = 1.0
    );


# (1) Load data.
# rsd <- loadRSDFromCSVs(csv_dir)
# ---------------------------------------------------------------------------
loadRSDFromCSVs <- function(csv_dir) {
    rsd <- list(info = NULL, m_ids = NULL, dihs = NULL);
    for (i in 1:3) {
        in_filename <- sprintf(paste(csv_dir, info_fileTemplates, sep = ""),
                                i);
        rsd$info[[i]] <- read.csv(in_filename);
    }
    for (i in 1:3) {
        mid_filename <- sprintf(paste(csv_dir, mids_fileTemplates, sep = ""),
                                i);
        rsd$m_ids[[i]] <- read.csv(mid_filename);
    }
    for (i in 1:2) {
        dih_filename <- sprintf(paste(csv_dir, dihs_fileTemplates, sep = ""),
                                i + 1);
        rsd$dihs[[i]] <- read.csv(dih_filename);
    }
    rsd;
}



# (2) Select training/testing data.
# training_sets <- selectTrainingSets(rsd, training, testing)
# ---------------------------------------------------------------------------
selectTrainingSets <- function(rsd, training, testing) {
    train_x <- NULL;
    train_y <- NULL;
    for (i in loop(length(training))) {
       train_x <- rbind(train_x, rsd$info[[ training[i] ]]);
       train_y <- rbind(train_y, rsd$dihs[[ training[i] ]]);
    }

    test_x <- NULL;
    for (i in loop(length(testing))) {
        test_x <- rbind(test_x, rsd$info[[ testing[i] ]]);
    }

    training_sets <- list(train_x = train_x, train_y = array(unlist(train_y)), 
                            test_x = array(unlist(test_x), dim = dim(test_x)));
    training_sets;
}



# (3) Undersample, multi-coverage
# cov_train_sets <- coverageTrainingSets(training_sets, min_coverage)
# ---------------------------------------------------------------------------
coverageTrainingSets <- function(training_sets, min_coverage) {
    hits <- which(training_sets$train_y > 0);
    misses <- which(training_sets$train_y == 0);

    num_hits <- length(hits);
    req_coverage <- length(misses) * min_coverage;
    coverages <- ceiling(req_coverage / (2 * num_hits));
    xes <- list();
    yes <- list();
    for (i in 1:coverages) {
        miss_sample <- sample(misses, num_hits);
        xes[[i]] <- training_sets$train_x[c(hits, miss_sample), ];
        xes[[i]] <- array(unlist(xes[[i]]), dim = dim(xes[[i]]));
        yes[[i]] <- training_sets$train_y[c(hits, miss_sample)];
    }

    cov_train_sets <- list(xes = xes, yes = yes,
                            test_x = training_sets$test_x);
    cov_train_sets;
}



# (4) PCA analyze each cov_train_set.
# cov_pc_loadings <- calculatePCALoadings(cov_train_sets, min_var)
# ---------------------------------------------------------------------------
calculatePCALoadings <- function(cts, min_var) {
    cov_pc_loadings <- list();
    for (i in 1:length(cts$xes)) {
        cov_pc_loadings[[i]] <- findPrincipalComponentLoading(cts$xes[[i]],
                                                                min_var);
        cat(sprintf("Coverage set using %d dimensions.\n", 
                        dim(cov_pc_loadings[[i]])[2]));
    }
    cov_pc_loadings;
}


# (5) Convert training data to PCA'ed reduced set.
# pc_train_sets <- applyPCALoadings(cov_train_sets, cov_pc_loadings)
# ---------------------------------------------------------------------------
applyPCALoadings <- function(cts, cpl) {
    red_xes <- list();
    for (i in 1:length(cpl)) {
        red_xes[[i]] <- cts$xes[[i]] %*% cpl[[i]];
    }

    pc_train_sets <- list(xes = red_xes, yes = cts$yes, test_x = cts$test_x);
    pc_train_sets;
}



# (6) Train Gradient Boosting Machines for each training set.
# gbms <- trainGBMs(pc_train_sets, gbm_parameters) {
# ---------------------------------------------------------------------------
trainGBMs <- function(pts, g_params) {
    gbms <- list();
    for (i in 1:length(pts$xes)) {
        gbms[[i]] <- gbm.fit(
            x               = pts$xes[[i]],
            y               = log1p(pts$yes[[i]]),
            distribution    = "gaussian",
            n.trees         = g_params$n_trees,
            shrinkage       = g_params$shrinkage,
            interaction.depth = g_params$depth,
            n.minobsinnode  = g_params$min_obs,
            bag.fraction    = g_params$bag_fraction,
            train.fraction  = g_params$train_fraction,
            verbose = TRUE
            );
        warnings();

        summary(gbms[[i]], g_params$n_trees);
    }

    gbms;
}



# (7) Use each gbm to predict the testing est (Rembmer to PCA Load test sets).
# predictions <- predictTestSet(pc_train_sets, cov_pc_loadings, gbms, g_params)
# ---------------------------------------------------------------------------
predictTestSet <- function(pts, cpl, gbms, g_params) {
    predictions <- list();
    for (i in 1:length(gbms)) {
        test_x <- pts$test_x;
        test_x <- test_x %*% cpl[[i]];

        predictions[[i]] <- predict.gbm(object = gbms[[i]],
                                    newdata = test_x,
                                    g_params$n_trees);
        predictions[[i]] <- transformPrediction(predictions[[i]]);
    }

    predictions;
}



# (8) Select median of predictions.
# medians <- selectMedian(predictions)
# ---------------------------------------------------------------------------
selectMedian <- function(predictions) {
    p_mat <- matrix(unlist(predictions), nc = length(predictions));
    medians <- apply(p_mat, 1, median);
    medians;
}




# ---------------------------------------------------------------------------
# undersampled gbm execution
# ---------------------------------------------------------------------------
# (1) Load data.
rsd <- loadRSDFromCSVs(csv_dir);

# (2) Select training/testing data.
training_sets <- selectTrainingSets(rsd, c(1, 2), 3);

# (3) Undersample, multi-coverage.
cov_train_sets <- coverageTrainingSets(training_sets, 2);

# (4) Principal component analyze each cts
cov_pc_loadings <- calculatePCALoadings(cov_train_sets, 0.95);

# (5) Convert training data to PCA'ed reduced set.
pc_train_sets <- applyPCALoadings(cov_train_sets, cov_pc_loadings);

# (6) Train Gradient Boosting Machines for each training set.
gbms <- trainGBMs(pc_train_sets, gbm_parameters);

# (7) Use each gbm to predict the testing set (Remember to PCA Load test sets)
predictions <- predictTestSet(
                        pc_train_sets, cov_pc_loadings, gbms, gbm_parameters)

# (8) Select Median of Coverage.
medians <- selectMedian(predictions);

# (9) Output to file.
writeSubmission(medians, rsd$m_ids[[3]]$MemberID, "ugbm");
