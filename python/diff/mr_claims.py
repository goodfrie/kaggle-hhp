#! /usr/bin/env python

"""
mr_claims.py
Scott Goodfriend - May 3, 2012
INFO 290 Project
"""
from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol
from numpy import array

import misc_mr

column = "LengthOfStay";

zero_blanks = 0;

class MR_ClaimsDiff(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def map_column(self, key, record):
        """
        Record -> <column, dih>
        """
        dihs = misc_mr.dih_array(record);
        c_index = [[], [], []];
        for claim in record['Claims']:
            year = claim['Year'];
            c_index[year - 1].append(claim[column]);

        c_score = [0, 0, 0];
        for i, indexes in enumerate(c_index):
            c_score[i] = max(indexes + [0]);

        for i, dih in enumerate(dihs):
            if dih >= 0:
                yield c_score[i], dih;
            elif zero_blanks:
                yield c_score[i], 0;


    def reduce_column(self, score, dih):
        """
        <score, dih> -> <score, cms_dih>
        """
        yield score, misc_mr.count_mu_sigma(dih);


    def steps(self):
        return [    self.mr(self.map_column, self.reduce_column)];



if __name__ == '__main__':
    MR_ClaimsDiff.run() # ../../../data.json | sort -nr -k3

"""
DSFS:
    12  [12258, 0.90928373307227928, 2.2698719726729233]
    11  [18288, 0.71932414698162728, 2.0210275138253846]
    10  [15333, 0.53244635752951153, 1.7092347976124029]
    9   [12342, 0.45584184086857882, 1.5722082387557437]
    8   [9935, 0.43925515853044789, 1.5758970408817314]
    7   [8517, 0.42479746389573791, 1.5069438421944161]
    6   [7802, 0.40861317610869008, 1.4832803629246709]
    5   [6765, 0.3549150036954915, 1.3078222266483532]
    4   [6628, 0.34942667471333738, 1.2771332646137441]
    2   [7908, 0.31765300961052101, 1.3296954500432607]
    3   [7056, 0.30612244897959184, 1.1791590352271091]
    1   [34606, 0.23530601629775183, 1.0576201847084612]
    0   [35, 0.14285714285714285, 0.42378277069118075]
"""
