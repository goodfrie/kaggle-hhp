"""
misc_mr.py
Scott Goodfriend - April 20, 2012
INFO 290 Project
"""

from numpy import array

def count_mu_sigma(gen):
    enum = array(list(gen));
    count = len(enum);
    if count:
        mean = enum.mean();
        std = enum.std();
    else:
        mean = 0;
        std = 0;
    return (count, mean, std);


def dih_array(record):
    dih2 = record['DaysInHospital_Y2'][0];
    dih3 = record['DaysInHospital_Y3'][0];
    return (dih2, dih3);
