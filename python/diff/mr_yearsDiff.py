#! /usr/bin/env python

"""
mr_yearsDiff.py
Scott Goodfriend - April 19, 2012
INFO 290 Project
"""
from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol
from numpy import array


indep_column = 'AgeAtFirstClaim';

dih2 = 'DaysInHospital_Y2';
dih3 = 'DaysInHospital_Y3';

class MR_YearsDiff(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def map_dihs(self, key, record):
        age = record[indep_column];
        d2 = record[dih2][0];
        d3 = record[dih3][0];
        if d2 >= 0:
            yield age, (1, d2);
        if d3 >= 0:
            yield age, (1, d3);


    def reduce_dihs(self, age, value):
        unzip = zip(*value);
        count = sum(unzip[0]);
        dihs = array(unzip[1]);
        yield age, (count, dihs.mean(), dihs.std());


    def steps(self):
        """
        map_dihs    : Record            -> <age, (1, dih)>
        reduce_dihs : <age, (1, dih)>   -> <age, (total #, dih_mean, dih_std)>
        """
        return [    self.mr(self.map_dihs, self.reduce_dihs)];



if __name__ == '__main__':
    MR_YearsDiff.run() # ../../data.json | sort -n -k1

"""
5   [12823, 0.16735553302659284, 0.69472085073648382]
15  [13004, 0.14856967087050138, 0.63822675391217021]
25  [8604, 0.40271966527196651, 1.2367182037023476]
35  [14206, 0.30480078839926794, 1.1411138535878043]
45  [29020, 0.45723638869745004, 1.5125016629545127]
55  [16847, 0.28171187748560572, 1.2551075784587953]
65  [17438, 0.47832320220208741, 1.7255377624006889]
75  [23643, 0.70892018779342725, 2.0923643994987335]
85  [11888, 0.99327052489905787, 2.3502144321355756]

The relationship isn't so hot. If you're very old you tend to go have a
higher DIH; however, what is going on is that the standard deviation goes up
as the person ages. This makes sense as older people have times where they
need extended hospitalization.
"""
