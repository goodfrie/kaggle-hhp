#! /usr/bin/env python

"""
mr_charlsonDiff.py
Scott Goodfriend - April 20, 2012
INFO 290 Project
"""

from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol

import misc_mr

run_max = 1;
zero_blanks = 1;

class MR_CharlsonDiff(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def map_charlson(self, key, record):
        """
        Record -> <charlson_int, dih>
        """
        dihs = misc_mr.dih_array(record);
        c_index = [[], [], []];
        for claim in record['Claims']:
            year = claim['Year'];
            c_index[year - 1].append(claim['CharlsonIndex']);
        
        c_score = [0, 0, 0];
        for i, indexes in enumerate(c_index):
            if run_max:
                c_score[i] = max(indexes + [0]);
            else:
                cms = misc_mr.count_mu_sigma(indexes);
                c_score[i] = int(round(cms[1]));

        for i, dih in enumerate(dihs):
            if dih >= 0:
                yield c_score[i], dih;
            else:
                if zero_blanks:
                    yield c_score[i], 0;


    def reduce_charlson(self, charlson, dih):
        """
        <charlson_int, dih> -> <charlson_int, cms_dih>
        """
        yield charlson, misc_mr.count_mu_sigma(dih);


    def steps(self):
        return [    self.mr(self.map_charlson, self.reduce_charlson)];


if __name__ == '__main__':
    MR_CharlsonDiff.run() # ../../data.json | sort -n -k1

"""
Average Charlson index:
    0   [98722, 0.30312392374546709, 1.1910371462815028]
    1   [10708, 0.62177810982443038, 1.8533808758519981]
    2   [36006, 0.77587068821863026, 2.1473860650958683]
    3   [1771, 1.1756070016939582, 2.7943221271830474]
    4   [219, 0.72146118721461183, 2.2915460399171077]
    5   [33, 1.0303030303030303, 2.3675181799544984]
    6   [14, 0.35714285714285715, 1.287696884094282]

    Kind of surprising. The relationship is not very good.
"""
"""
Max Charlson index:
    0   [96207, 0.29594520149261488, 1.169636081981871]
    2   [45467, 0.68099940616271137, 1.9763887430612768]
    4   [5053, 1.2576687116564418, 2.8400361844311766]
    6   [746, 1.3512064343163539, 2.9950772645552366]

    This is much better actually.
"""
