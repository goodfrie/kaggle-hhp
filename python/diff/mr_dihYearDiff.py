#! /usr/bin/env python

"""
mr_dihYearDiff.py
Scott Goodfriend - April 19, 2012
INFO 290 Project
"""
from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol
from numpy import array

dih2 = "DaysInHospital_Y2";
dih3 = "DaysInHospital_Y3";

class MR_DIHYearDiff(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def mapper(self, key, record):
        """
        Record -> <year, days_in_hospital>
        """
        d2 = record[dih2][0];
        d3 = record[dih3][0];
        if d2 >= 0:
            yield 2, d2;
        if d3 >= 0:
            yield 3, d3;


    def reducer(self, year, dih):
        """
        <year, dih> -> <year, (total #, dih_mean, dih_std)>
        """
        dihs = array(list(dih));
        count = len(dihs);
        if count:
            mean = dihs.mean();
            std = dihs.std();
        else:
            mean = 0;
            std = 0;
        yield year, (count, mean, std);



if __name__ == '__main__':
    MR_DIHYearDiff.run() # ../../data.json | sort -n -k1

"""
2   [76038, 0.46709539966858676, 1.6121838959328856]
3   [71435, 0.43789458948694615, 1.5317695330030925]

Nearly no difference. If there was a differene I would be concerned.
"""
