#! /usr/bin/env python

"""
hhp_io.py
Scott Goodfriend - April 14, 2012 - Revision 1
INFO 290 Project
"""

import csv
import json
import os

import hhp_cmd


def readMembers(filename):
    members = dict();

    f = open(filename, "r");
    reader = csv.DictReader(f);
    for row in reader:
        member_id = row['MemberID'];
        age = row['AgeAtFirstClaim'];
        sex = row['Sex'];
        members[member_id] = {
                'MemberID': member_id,
                'AgeAtFirstClaim': age,
                'Sex': sex,
                'Claims': [],
                'LabCount': [],
                'DrugCount': [],
                'DaysInHospital_Y2': [-1],
                'ClaimsTruncated_Y2': -1,
                'DaysInHospital_Y3': [-1],
                'ClaimsTruncated_Y3': -1,
                'DaysInHospital_Y4': [-1],
                'ClaimsTruncated_Y4': -1,
                };
    f.close();

    return members;


def combineMembers_Claims(members, filename):
    f = open(filename, "r");
    reader = csv.DictReader(f);
    for row in reader:
        member_id = row['MemberID'];
        del row['MemberID']
        members[member_id]['Claims'].append(row);
    f.close();


def combineMembers_LabCount(members, filename):
    f = open(filename, "r");
    reader = csv.DictReader(f);
    for row in reader:
        member_id = row['MemberID'];
        del row['MemberID'];
        members[member_id]['LabCount'].append(row);
    f.close();


def combineMembers_DrugCount(members, filename):
    f = open(filename, "r");
    reader = csv.DictReader(f);
    for row in reader:
        member_id = row['MemberID'];
        del row['MemberID'];
        members[member_id]['DrugCount'].append(row);
    f.close();


def combineMembers_DIH(members, directory, name):
    f = open(directory + '/' + name + '.csv', "r");
    reader = csv.DictReader(f);
    for row in reader:
        member_id = row['MemberID'];
        claims_truncated = row['ClaimsTruncated'];
        dih = row['DaysInHospital'];
        members[member_id][name][0] = dih;
        members[member_id]['ClaimsTruncated_Y' + name[-1]] \
                = claims_truncated;
    f.close();


def combineMembers_Target(members, filename):
    f = open(filename, "r");
    reader = csv.DictReader(f);
    for row in reader:
        member_id = row['MemberID'];
        members[member_id]['ClaimsTruncated_Y4']  = row['ClaimsTruncated'];
    f.close();


def collateMemberData(directory):
    member_dict = readMembers(directory + "/Members.csv");
    combineMembers_Claims(member_dict, directory + "/Claims.csv");
    combineMembers_LabCount(member_dict, directory + "/LabCount.csv");
    combineMembers_DrugCount(member_dict, directory + "/DrugCount.csv");
    combineMembers_DIH(member_dict, directory, "DaysInHospital_Y2");
    combineMembers_DIH(member_dict, directory, "DaysInHospital_Y3");
    combineMembers_Target(member_dict, directory + "/Target.csv");
    return member_dict;


def writeMemberData(members, out_filename):
    w = open(out_filename, "w");
    for key in members:
        row = members[key];
        out_str = json.dumps(row);
        w.write(out_str + os.linesep);
    w.flush();
    w.close();


def convertMemberData(filename, out_filename):
    f = open(filename, "r");
    w = open(out_filename, "w");
    for line in f:
        member = json.loads(line);
        row = hhp_cmd.cleanMemberData(member);
        out = json.dumps(row);
        w.write(out + '\n');
    f.close();
    w.flush();
    w.close();


def abridgedData(filename, abridged_filename, num):
    f = open(filename, "r");
    w = open(abridged_filename, "w");
    i = 0;
    for line in f:
        member = json.loads(line);
        out = json.dumps(member);
        w.write(out + '\n');
        i = i + 1;
        if i >= num:
            break;
    f.close();
    w.flush();
    w.close();


def readMemberData(filename):
    f = open(filename, "r");
    members = json.load(f);
    f.close();
    return members;



def writeTarget(members, target_filename, out_filename):
    f = open(target_filename, "r");
    w = open(out_filename, "w");
    reader = csv.DictReader(f);
    writer = csv.DictWriter(w, reader.fieldnames);
    writer.writeheader();
    for row in reader:
        row['DaysInHospital'] \
                = members[row['MemberID']]['DaysInHospital_Y4'][0];
        writer.writerow(row);
    w.flush();
    w.close();
    f.close();



if __name__ == '__main__':
    members = collateMemberData("../../HHP_release3");
    writeMemberData(members, "../../HHP3.json");
    convertMemberData("../../HHP3.json", "../../data.json");
    abridgedData("../../data.json", "../../short.json", 1000);
