#! /usr/bin/env python

"""
pred_averageTwoYears.py
Scott Goodfriend - April 14, 2012 - Revision 1
INFO 290 Project
"""

import json

import hhp_io


def averageTwoYears(members):
    """
    Iterate through all members, taking the average of Y2 and Y3 to calculate
    Y4. If no information, assume 0.
    """
    for m_id in members:
        member = members[m_id];
        dih_Y2 = int(member['DaysInHospital_Y2'][0]);
        if dih_Y2 == -1:
            dih_Y2 = 0;
        dih_Y3 = int(member['DaysInHospital_Y3'][0]);
        if dih_Y3 == -1:
            dih_Y3 = 0;
        member['DaysInHospital_Y4'][0] = int(float(dih_Y2 + dih_Y3) / 2);



if __name__ == '__main__':
    members = hhp_io.readMemberData('../../HHP3.json');
    averageTwoYears(members);
    hhp_io.writeTarget( members, \
                        '../../HHP_release3/Target.csv', \
                        '../../targets/Target.csv');
