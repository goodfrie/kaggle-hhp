#! /usr/bin/env python

"""
hhp_cmd.py
Scott Goodfriend - April 15, 2012 - Revision 1
INFO 290 Project

A lot of values are similar to those used from Market Makers' First Milestone
paper.
"""

age_dict = {
        "0-9"   :   5,  # 10791
        "10-19" :   15, # 11319
        "20-29" :   25, # 8505
        "30-39" :   35, # 12435
        "40-49" :   45, # 16111
        "50-59" :   55, # 13329
        "60-69" :   65, # 12622
        "70-79" :   75, # 14514
        "80+"   :   85, # 7621
        ""      :   50  # 5753; picking mode value for unknown patients.
    }
def convertAgeToInt(age_str):
    return age_dict[age_str];


sex_dict = {
        "F"     :   -1, # 51482
        "M"     :   1,  # 43966
        ""      :   0   # 17552; picking in between.
    }
def convertSex(sex_str):
    return sex_dict[sex_str];


# --- Claim Conversion ---
def convertClaims(claims):
    l_claims = list();
    for c in claims:
        l_claims.append(convertClaim(c));
    return l_claims;


def convertClaim(claim):
    c = dict();
    c['ProviderID'] = convertProviderID(claim['ProviderID']);
    c['Vendor'] = convertVendor(claim['Vendor']);
    c['PCP'] = convertPCP(claim['PCP']);
    c['Year'] = int(claim['Year'][1]);
    c['Specialty'] = convertSpecialty(claim['Specialty']);
    c['PlaceSvc'] = convertPlaceSvc(claim['PlaceSvc']);
    c['PayDelay'] = convertPayDelay(claim['PayDelay']);
    c['LengthOfStay'] = convertLengthOfStay(claim['LengthOfStay']);
    c['DSFS'] = convertDSFS(claim['DSFS']);
    c['PrimaryConditionGroup'] = convertPCG(claim['PrimaryConditionGroup']);
    c['CharlsonIndex'] = convertCharlsonIndex(claim['CharlsonIndex']);
    c['ProcedureGroup'] = convertProcedureGroup(claim['ProcedureGroup']);
    c['SupLOS'] = convertSupLOS(claim['SupLOS']);
    return c;


def convertProviderID(p_id):
    if p_id == "":
        return 0;
    else:
        return int(p_id);


def convertVendor(vendor):
    if vendor == "":
        return 0;
    else:
        return int(vendor);


def convertPCP(pcp):
    if pcp == "":
        return 0;
    else:
        return int(pcp);


specialty_dict = {
        "Anesthesiology"            : 0,    # 33435
        "Diagnostic Imaging"        : 1,    # 207297
        "Emergency"                 : 2,    # 126130
        "General Practice"          : 3,    # 473655
        "Internal"                  : 4,    # 672059
        "Laboratory"                : 5,    # 653188
        "Obstetrics and Gynecology" : 6,    # 36594
        "Pathology"                 : 7,    # 14907
        "Pediatrics"                : 8,    # 84862
        "Rehabilitation"            : 9,    # 57554
        "Surgery"                   : 10,   # 208217
        "Other"                     : 11,   # 92687
        ""                          : 12,   # 8405
    };
def convertSpecialty(claim_str):
    return specialty_dict[claim_str];


place_list = [
        "Ambulance",            #  34766
        "Home",                 # 8303
        "Independent Lab",      # 657750
        "Inpatient Hospital",   # 85776
        "Office",               # 1542007
        "Outpatient Hospital",  # 121528
        "Urgent Care",          # 199528
        "Other",                # 11700
        "",                     # 7632
    ];
place_dict = dict(zip(place_list, range(len(place_list))));
def convertPlaceSvc(placesvc):
    return place_dict[placesvc];


def convertPayDelay(pay_delay):
    if pay_delay == "162+":
        return 162;
    else:
        return int(pay_delay);


los_dict = {
        "1 day"         : 1,    # 56696
        "2 days"        : 2,    # 6485
        "3 days"        : 3,    # 3246
        "4 days"        : 4,    # 1473
        "5 days"        : 5,    # 510
        "6 days"        : 6,    # 179
        "1- 2 weeks"    : 11,   # 1143
        "2- 4 weeks"    : 21,   # 961
        "4- 8 weeks"    : 42,   # 903
        "26+ weeks"     : 180,  # 2
        ""              : 0,    # 2597392
    };
def convertLengthOfStay(length_of_stay):
    return los_dict[length_of_stay];


dsfs_list = [
        "",              # 52770 
        "0- 1 month",    # 707721
        "1- 2 months",   # 247343
        "2- 3 months",   # 225216
        "3- 4 months",   # 212214
        "4- 5 months",   # 189001
        "5- 6 months",   # 192000
        "6- 7 months",   # 180662
        "7- 8 months",   # 175191
        "8- 9 months",   # 171878
        "9-10 months",   # 151527
        "10-11 months",  # 116328
        "11-12 months",  # 47139
    ]
dsfs_dict = dict(zip(dsfs_list, range(len(dsfs_list))));
def convertDSFS(dsfs):
    return dsfs_dict[dsfs];


pcg_list = [
        "AMI",                  # 34805     1
        "APPCHOL",		# 17945     2
        "ARTHSPIN",             # 288285    3
        "CANCRA",		# 5587      4
        "CANCRB",		# 42895     5
        "CANCRM",		# 1096      6
        "CATAST",		# 2070      7   
        "CHF",		        # 13316     8
        "COPD",                 # 44154     9
        "FLaELEC",		# 5263      10
        "FXDISLC",		# 40851     11
        "GIBLEED",		# 101846    12
        "GIOBSENT",             # 9718      13
        "GYNEC1",		# 44143     14
        "GYNECA",		# 12491     15
        "HEART2",		# 54207     16
        "HEART4",		# 28733     17
        "HEMTOL",		# 31631     18
        "HIPFX" ,               # 4355      19
        "INFEC4",		# 83552     20
        "LIVERDZ",		# 2747      21
        "METAB1",		# 3863      22
        "METAB3",		# 320553    23
        "MISCHRT",		# 131047    24
        "MISCL1",		# 4892      25
        "MISCL5",		# 48307     26
        "MSC2a3",		# 507277    27
        "NEUMENT",		# 171605    28
        "ODaBNCA",		# 46732     29
        "PERINTL",		# 980       30
        "PERVALV",		# 3518      31
        "PNCRDZ",		# 912       32
        "PNEUM",                # 11333     33
        "PRGNCY",		# 32004     34
        "RENAL1",		# 602       35
        "RENAL2",		# 10922     36
        "RENAL3",		# 52214     37
        "RESPR4",		# 138062    38
        "ROAMI",                # 48821     39
        "SEIZURE",		# 20501     40
        "SEPSIS",		# 497       41
        "SKNAUT",		# 107976    42
        "STROKE",		# 8416      43
        "TRAUMA",		# 72050     44
        "UTI",		        # 44806     45
        "",                     # 11410     46
    ];
pcg_dict = dict(zip(pcg_list, range(len(pcg_list))));
def convertPCG(pcg):
    return pcg_dict[pcg];

charlson_dict = {
        "0"     : 0, # 1356995
        "1-2"   : 2, # 1256527
        "3-4"   : 4, # 49479
        "5+"    : 6, # 5989
    };
def convertCharlsonIndex(ci):
    return charlson_dict[ci];


procedure_list = [
        "ANES", # 17061
        "EM",   # 1048210
        "MED",  # 372101
        "PL",   # 492919
        "RAD",  # 265272
        "SAS",  # 5745
        "SCS",  # 274805
        "SDS",  # 60678
        "SEOA", # 8420
        "SGS",  # 9406
        "SIS",  # 56461
        "SMCD", # 3376
        "SMS",  # 29177
        "SNS",  # 7796
        "SO",   # 371
        "SRS",  # 7905
        "SUS",  # 5612
        "",     # 3675
    ]
procedure_dict = dict(zip(procedure_list, range(len(procedure_list))));
def convertProcedureGroup(pg):
    return procedure_dict[pg];


    c['SupLOS'] = convertSupLOS(claim['SupLOS']);
def convertSupLOS(los):
    return int(los);



# --- Lab Count Conversion ---
def convertLabCounts(labs):
    ls = list();
    for l in labs:
        ls.append(convertLabCount(l));
    return ls;


def convertLabCount(lab):
    ldict = dict();
    ldict['Year'] = int(lab['Year'][1]);
    ldict['DSFS'] = convertDSFS(lab['DSFS']);
    ldict['LabCount'] = convertLC(lab['LabCount']);
    return ldict;


def convertLC(lab_count):
    if lab_count == "10+":
        return 10;
    else:
        return int(lab_count);



# --- Drug Count Converstion ---
def convertDrugCounts(drugs):
    ld = list();
    for d in drugs:
        ld.append(convertDrugCount(d));
    return ld;


def convertDrugCount(drug):
    ddict = dict();
    ddict['Year'] = int(drug['Year'][1]);
    ddict['DSFS'] = convertDSFS(drug['DSFS']);
    ddict['DrugCount'] = convertDC(drug['DrugCount']);
    return ddict;


def convertDC(drug_count):
    if drug_count == "7+":
        return 7;
    else:
        return int(drug_count);



# --- Major Conversion ---
def cleanMemberData(member_row):
    member = dict();
    member['MemberID'] = int(member_row['MemberID']);
    member['AgeAtFirstClaim'] = convertAgeToInt(member_row['AgeAtFirstClaim']);
    member['Sex'] = convertSex(member_row['Sex']);
    member['Claims'] = convertClaims(member_row['Claims']);
    member['LabCount'] = convertLabCounts(member_row['LabCount']);
    member['DrugCount'] = convertDrugCounts(member_row['DrugCount']);
    for y in ['2', '3', '4']:
        dstr = 'DaysInHospital_Y' + y;
        cstr = 'ClaimsTruncated_Y' + y;
        member[dstr] = list();
        for i in range(len(member_row[dstr])):
            member[dstr].append(int(member_row[dstr][i]));
        member[cstr] = int(member_row[cstr]);
    return member;



if __name__ == '__main__':
    import hhp_io
    hhp_io.convertMemberData("../../HHP3.json", "../../data.json");
