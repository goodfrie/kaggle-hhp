#! /usr/bin/env python

"""
mr_twoHisto.py
Scott Goodfriend - April 19, 2012
INFO 290 Project
"""
from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol

sheet = "DrugCount"
column = "DrugCount"

class MR_TwoHisto(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def mapper(self, key, record):
        for page in record[sheet]:
            yield page[column], 1;

    def reducer(self, name, counts):
        yield name, sum(counts);



if __name__ == '__main__':
    MR_TwoHisto.run() # | sort -k1

"""
LabCount
"1"     93744
"2"     54183
"3"     43472
"4"     38815
"5"     34900
"6"     27705
"7"     20591
"8"     14885
"9"     10079
"10+"   23110
"""

"""
DrugCount
"1"     263501
"2"     188559
"3"     129881
"4"     87783
"5"     57768
"6"     36731
"7+"    54018
"""
