#! /usr/bin/env python

"""
mr_claimsHisto.py
Scott Goodfriend - April 18, 2012
INFO 290 Project
"""
from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol
import sys

category = "SupLOS"

class MR_ClaimsHisto(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;
        
    def mapper(self, key, record):
        for claim in record['Claims']:
            yield claim[category], 1;

    def reducer(self, name, counts):
        yield name, sum(counts);



if __name__ == '__main__':
    MR_ClaimsHisto.run() # | sort -k1

"""
PlaceSvc
""  7632
"Ambulance" 34766
"Home"  8303
"Independent Lab"   657750
"Inpatient Hospital"    85776
"Office"    1542007
"Other" 11700
"Outpatient Hospital"   121528
"Urgent Care"   199528
"""

"""
LengthOfStay
""  2597392
"1 day" 56696
"1- 2 weeks"    1143
"2 days"    6485
"2- 4 weeks"    961
"26+ weeks" 2
"3 days"    3246
"4 days"    1473
"4- 8 weeks"    903
"5 days"    510
"6 days"    179
"""

"""
DSFS
""  52770
"0- 1 month"    707721
"1- 2 months"   247343
"10-11 months"  116328
"11-12 months"  47139
"2- 3 months"   225216
"3- 4 months"   212214
"4- 5 months"   189001
"5- 6 months"   192000
"6- 7 months"   180662
"7- 8 months"   175191
"8- 9 months"   171878
"9-10 months"   151527
"""

"""
PrimaryConditionGroup
""  11410
"AMI"   34805
"APPCHOL"   17945
"ARTHSPIN"  288285
"CANCRA"    5587
"CANCRB"    42895
"CANCRM"    1096
"CATAST"    2070
"CHF"   13316
"COPD"  44154
"FLaELEC"   5263
"FXDISLC"   40851
"GIBLEED"   101846
"GIOBSENT"  9718
"GYNEC1"    44143
"GYNECA"    12491
"HEART2"    54207
"HEART4"    28733
"HEMTOL"    31631
"HIPFX" 4355
"INFEC4"    83552
"LIVERDZ"   2747
"METAB1"    3863
"METAB3"    320553
"MISCHRT"   131047
"MISCL1"    4892
"MISCL5"    48307
"MSC2a3"    507277
"NEUMENT"   171605
"ODaBNCA"   46732
"PERINTL"   980
"PERVALV"   3518
"PNCRDZ"    912
"PNEUM" 11333
"PRGNCY"    32004
"RENAL1"    602
"RENAL2"    10922
"RENAL3"    52214
"RESPR4"    138062
"ROAMI" 48821
"SEIZURE"   20501
"SEPSIS"    497
"SKNAUT"    107976
"STROKE"    8416
"TRAUMA"    72050
"UTI"   44806
"""

"""
CharlsonIndex
"0" 1356995
"1-2"   1256527
"3-4"   49479
"5+"    5989
"""

"""
ProcedureGroup
""  3675
"ANES"  17061
"EM"    1048210
"MED"   372101
"PL"    492919
"RAD"   265272
"SAS"   5745
"SCS"   274805
"SDS"   60678
"SEOA"  8420
"SGS"   9406
"SIS"   56461
"SMCD"  3376
"SMS"   29177
"SNS"   7796
"SO"    371
"SRS"   7905
"SUS"   5612
"""

"""
SupLOS
"0" 2657658
"1" 11332
"""
