#! /usr/bin/env python

"""
mr_sexHisto.py
Scott Goodfriend - April 16, 2012 - Revision 1
INFO 290 Project
"""

from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol


class MR_SexHisto(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def map_sexes(self, key, record):
        yield record['Sex'], 1;

    def reduce_sexes(self, sex, counts):
        yield sex, sum(counts);


    def steps(self):
        """
        map_sexes       : Record    -> <sex, 1>
        reduce_sexes    : <sex, 1>  -> <sex, total #>
        """
        return [    self.mr(self.map_sexes, self.reduce_sexes)];



if __name__ == '__main__':
    MR_SexHisto.run() # | sort -k1

"""
""  17552
"F" 51482
"M" 43966
"""
