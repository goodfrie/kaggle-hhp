#! /usr/bin/env python

"""
mr_ageHisto.py
Scott Goodfriend - April 15, 2012 - Revision 1
INFO 290 Project
"""

from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol


class MR_AgeHisto(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def map_ages(self, key, record):
        yield record['AgeAtFirstClaim'], 1;

    def reduce_ages(self, ages, counts):
        yield ages, sum(counts);

    def steps(self):
        """
        map_ages    : Record    -> <age, 1>
        reduce_ages : <age, 1>  -> <age, total #>
        """
        return [self.mr(self.map_ages, self.reduce_ages)];


if __name__ == '__main__':
    MR_AgeHisto.run() # | sort -r -k1

"""
"0-9"   10791
"10-19" 11319
"20-29" 8505
"30-39" 12435
"40-49" 16111
"50-59" 13329
""  5753
"60-69" 12622
"70-79" 14514
"80+"   7621
"""
