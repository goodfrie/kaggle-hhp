#! /usr/bin/env python

"""
mr_specialtyHisto.py
Scott Goodfriend - April 16, 2012 - Revision 1
INFO 290 Project
"""
from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol


class MR_SpecialtyHisto(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol;

    def map_specialties(self, key, record):
        for claim in record['Claims']:
            yield claim['Specialty'], 1;

    def reduce_specialties(self, specialty, counts):
        yield specialty, sum(counts);


    def steps(self):
        """
        map_specialties     : Record            -> <specialty, 1>
        reduce_specialties  : <specialty, 1>    -> <specialty, total #>
        """
        return [self.mr(self.map_specialties, self.reduce_specialties)];



if __name__ == '__main__':
    MR_SpecialtyHisto.run() # | sort -k1

"""
""  8405
"Anesthesiology"    33435
"Diagnostic Imaging"    207297
"Emergency" 126130
"General Practice"  473655
"Internal"  672059
"Laboratory"    653188
"Obstetrics and Gynecology" 36594
"Other" 92687
"Pathology" 14907
"Pediatrics"    84862
"Rehabilitation"    57554
"Surgery"   208217
"""
